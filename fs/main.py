import logging

from fastapi import FastAPI, Depends, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import RedirectResponse

from . import models
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)
app = FastAPI()
# set logging output to file
logging.basicConfig(filename="fs.log", level=logging.INFO)

def get_db():
	db = SessionLocal()
	try:
		yield db
	finally:
		db.close()

from .lru import Lru, Elem
pos_map = {}
lru = Lru(50*1024*1024) # 50MB

def init():
	db = SessionLocal()
	global lru
	lru = Lru(50*1024*1024) # 50MB
	metadatas = db.query(models.Metadata)
	for metadata in metadatas:
		if metadata.position == "10.193.235.228":
			lru.put(Elem(metadata.size, metadata.filename))

@app.get("/")
def read_root():
	return {"Hello": "World"}

app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/metadata/{filename}")
def read_metadata(filename: str, db = Depends(get_db)):
	metadata = db.query(models.Metadata).filter(models.Metadata.filename == filename).first()
	return metadata

@app.get("/reset")
def reset(db = Depends(get_db)):
	import fs.scripts.scripts as scripts
	scripts.random_position(addresses=["10.128.48.35"]*9+["10.193.235.228"])
	init()
	return {"status": "ok"}

# mete server only
@app.get("/file/{filename}")
def read_file(filename: str, request: Request, db = Depends(get_db)):
	alg = request.query_params.get("alg")
	logging.info(f"Request for {filename} from {request.client.host}")
	source_ip = request.client.host
	metadata = db.query(models.Metadata).filter(models.Metadata.filename == filename).first()
	logging.info(f"Request for {metadata}")
	# update metadata
	# todo: update last_access_at, access_count
	ret = {}
	if metadata.position == source_ip:
		logging.info(f"Redirecting to localhost")
		ret = {"addr": f"http://127.0.0.1:8000/static/{filename}"}
	else :
		logging.info(f"Redirecting to {metadata.position}")
		ret = {"addr": f"http://{metadata.position}:8000/static/{filename}"}
	if alg == "lru":
		ret_list = lru.put(Elem(metadata.size, metadata.filename))
		for elem in ret_list:
			logging.info(f"Evicting {elem.filename} to cloud server")
			db.query(models.Metadata).filter(models.Metadata.filename == elem.filename).update({"position": "10.128.48.35"})
		# update database, 
		db.query(models.Metadata).filter(models.Metadata.filename == filename).update({"position": source_ip})
		db.commit()
	return ret

# uvicorn fs.main:app --host=0.0.0.0 --port=8000