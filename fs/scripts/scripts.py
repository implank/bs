# generate 1000 files random range from 10B to 1MB
# and then put into database
import os
import random
import sqlalchemy
import time

def gen_files(n = 1000):
	random.seed(0)
	total_size = 0
	for i in range(n):
		size = random.randint(10, 1024 * 1024)
		total_size += size
		filename = f"{i}.txt"
		with open(f"static/{filename}", "wb") as f:
			f.write(os.urandom(size))
	print(f"Total size: {total_size} bytes, {total_size / 1024 / 1024} MB")

def random_position(n = 1000, addresses = None):
	# write to database
	from sqlalchemy.orm import Session
	from sqlalchemy import update
	import fs.models as models
	from fs.database import SessionLocal, engine
	models.Base.metadata.create_all(bind=engine)
	db: Session = SessionLocal()
	random.seed(0)
	db.query(models.Metadata).filter(models.Metadata.filename.like("%.txt")).delete()
	# size_map = {0 for add in addresses}
	size_map = {add:0 for add in addresses}
	for i in range(n):
		filename = f"{i}.txt"
		size = os.path.getsize(f"static/{filename}")
		position = random.choice(addresses)
		metadata = models.Metadata(filename=filename, 
														 size=size, position=position, 
														 access_count=0, last_access_at=int(time.time()))
		size_map[position] += size
		db.add(metadata)
		db.commit()
	print(size_map)

	

gen_files()
"""
台式机（云）
10.128.48.35
笔记本ip：（边）
10.193.235.228
"""
random_position(addresses=["10.128.48.35"]*9+["10.193.235.228"])