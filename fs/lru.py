import os
from collections import deque
import threading

class Elem:
	size: int
	filename: str
	def __init__(self, size: int, filename: str):
		self.size = size
		self.filename = filename

	def __eq__(self, value: object) -> bool:
		if not isinstance(value, Elem):
			return False
		return self.filename == value.filename

class Lru:
	deq: deque 
	max_size: int
	now_size: int
	lock: threading.Lock
	def __init__(self, max_size: int):
		self.deq = deque()
		self.lock = threading.Lock()
		self.now_size = 0
		self.max_size = max_size
		assert self.now_size <= self.max_size

	def put(self, elem: Elem) -> list[Elem]:
		with self.lock:
			ret_list = []
			if elem in self.deq:
				self.deq.remove(elem)
			if elem.size + self.now_size > self.max_size:
				while elem.size + self.now_size > self.max_size:
					elem = self.deq.popleft()
					self.now_size -= elem.size
					ret_list.append(elem)
			self.deq.append(elem)
			self.now_size += elem.size
			return ret_list