from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


class Metadata(Base):
	__tablename__ = "metedata"

	filename = Column(String, primary_key=True)
	size = Column(Integer)
	last_access_at = Column(Integer)
	position = Column(String)
	access_count = Column(Integer)
