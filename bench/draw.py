# load data from job_status.csv and draw the graph in matplotlib

import matplotlib.pyplot as plt
import pandas as pd

def draw_job_status(filenames = []):
	for filename in filenames:
		df = pd.read_csv(filename)
		# x = cnt, y = avg_delay
		plt.plot(df["cnt"], df["avg_delay"])
	plt.xlabel("req_cnt")
	plt.ylabel("avg_latency")
	plt.show()

csvs = [
	"rd1.csv",
	"aedge1.csv"
]

draw_job_status(csvs)

