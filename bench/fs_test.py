import requests
import time
from concurrent.futures import ThreadPoolExecutor
import queue
import numpy as np
import threading

job_status = queue.Queue()
job_done = False

np.random.seed(0)

def get_file(filename):
	url = f"http://10.128.48.35:8000/file/{filename}?alg=lru"
	bg_t = time.time()
	r = requests.get(url)
	addr = r.json()["addr"]
	size = 0 
	if addr.startswith("http://127.0"):
		with open(f"static/{filename}", "rb") as f:
			buf = []
			while buf := f.read(1024*1024):
				size += len(buf)
	else:
		r = requests.get(addr)
		size = len(r.content)
	ed_t = time.time()
	print(f"filename: {filename}, Addr: {addr}, Time: {ed_t - bg_t}, from {bg_t} to {ed_t}")
	job_status.put({
		"filename": filename,
		"size(byte)": size,
		"addr": addr,
		"bg_t": bg_t,
		"ed_t": ed_t,
		"time": ed_t - bg_t
	})

def calc_job_delay(interval = 10):
	cnt = 0
	total_delay = 0
	with open(f"job_status_{str(time.time())}.csv", "w") as f, open(f"job_status2_{str(time.time())}.csv", "w") as f2:
		f.write("filename,size(byte),addr,bg_t,ed_t,time\n")
		f2.write("cnt,avg_delay\n")
		while True:
			try:
				job = job_status.get(timeout=1)
				f.write(f"{job['filename']},{job['size(byte)']},{job['addr']},{job['bg_t']},{job['ed_t']},{job['time']}\n")
				cnt += 1
				total_delay += job["time"]
				if cnt % interval == 0:
					f2.write(f"{cnt},{total_delay / interval}\n")
					total_delay = 0
			except queue.Empty:
				if job_done:
					break


def test_fs_norm(n = 1000):
	global job_done
	job_done = False
	threading.Thread(target=calc_job_delay).start()

	bg_t = time.time()
	tpe = ThreadPoolExecutor(max_workers=10)
	for i in range(n):
		tpe.submit(get_file, f"{i}.txt")
	
	# wait tpe to finish
	tpe.shutdown(wait=True)
	ed_t = time.time()
	print(f"Total time: {ed_t - bg_t}, from {bg_t} to {ed_t}")
	job_done = True

def test_fs_zipf(file_n = 1000, req_n = 100000):
	global job_done
	job_done = False
	threading.Thread(target=calc_job_delay).start()

	bg_t = time.time()
	tpe = ThreadPoolExecutor(max_workers=10)
	req_list = np.random.zipf(1.5, req_n*2)
	print(req_list[0:100])
	i = 0
	while i < req_n:
		req = req_list[i]
		i += 1
		# print(req)
		if req >= file_n:
			continue
		tpe.submit(get_file, f"{req}.txt")
	
	# wait tpe to finish
	tpe.shutdown(wait=True)
	ed_t = time.time()
	print(f"Total time: {ed_t - bg_t}, from {bg_t} to {ed_t}, req_n: {i}")
	job_done = True

# test_fs_norm()
test_fs_zipf(req_n=10000)

# req_list = np.random.zipf(1.5, 100000*2)
# print(req_list[0:100])