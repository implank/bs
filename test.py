from fs.lru import Lru, Elem

def test_lru():
	elems = [Elem(1, "a"), Elem(2, "b"), Elem(3, "c"), Elem(1, "a")]
	lru = Lru(10)
	for elem in elems:
		lru.put(elem)
		for e in lru.deq:
			print(e.filename, e.size)
		print("----")

test_lru()